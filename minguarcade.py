#!/usr/bin/python3
"""A standalone Raspberry Pi application for an arcade console. It cycles
through videos as people insert coins. The videos that it plays are found
in the /videos/ directory.
Written for Mingu for by mike@kocachic.com"""
from threading import Lock, Thread
import datetime
from subprocess import Popen, PIPE
from enum import Enum
import tty
import sys

VIDEOS = "/videos"

video_lock = Lock()
video_pid = None
video_num = 0
videos = []
for vid_n in [1, 2, 3]:
    videos.append((('%s/%d-1.mp4' % (VIDEOS, vid_n)), True))
    videos.append((('%s/%d-2.mp4' % (VIDEOS, vid_n)), False))

def print_ts(message):
    """Prints a message to standard output, along with a timestamp.

    :param message: the message to print"""
    print('%s\t%s' % (datetime.datetime.now(), message))

def coin_inserted():
    """A callback function, executed in a separate thread whenever
    a coin is inserted. We kill the currently playing video."""
    print_ts('Coin inserted')
    video_lock.acquire()
    print_ts('Stopping video player at PID %d...' % video_pid.pid)
    video_pid.communicate(input=b'q')	# omxplayer command to quit
    video_lock.release()

class VideoFinished(Enum):
    """An enumeration to represent the various ways the video
    player could have finished."""
    VIDEO_FINISHED = 0
    COIN_INSERTED = 1
    NEEDS_RETRY = 2

def play_video(filename, should_loop):
    """Plays a video using an external video player, and waits
    for it to finish.

    :param filename: the filename of the video to play
    :param should_loop: a boolean, indicating whether the video
        loops or not
    :returns: VIDEO_FINISHED or COIN_INSERTED"""
    args = ['omxplayer', '-b', '--no-osd', '-I', '-o', 'local']
    # actual video is 336x249, but omxplayer doesn't respect the overscan
    # in /boot/config.txt
    args.extend(['--win', '16 60 304 220'])
    if should_loop:
        args.append('--loop')
    args.append(filename)
    video_lock.acquire()
    print_ts('Playing video %s' % filename)
    global video_pid
    video_pid = Popen(args, stdin=PIPE, bufsize=0)
    video_lock.release()

    video_pid.wait()

    video_lock.acquire()
    if video_pid.returncode == 0:   # video player finished successfully
        print_ts('Video %s finished normally' % filename)
        how = VideoFinished.VIDEO_FINISHED
    elif video_pid.returncode == 3: # was stopped
        print_ts('Video %s was killed by a coin insert. Moving to the next...' % filename)
        how = VideoFinished.COIN_INSERTED
    else:
        print_ts('Video %s crashed or was killed: returned %d' % (filename, video_pid.returncode))
        how = VideoFinished.NEEDS_RETRY
    video_lock.release()
    return how

def play_next_video():
    """Determines the filename, etc., of which video to play
    next, then calls play_video() to play it."""
    global video_num
    try:
        video_lock.acquire()
        filename, should_loop = videos[video_num]
        video_lock.release()
        how_exited = play_video(filename, should_loop)
        video_lock.acquire()
        if how_exited != VideoFinished.NEEDS_RETRY:
            video_num = (video_num + 1) % len(videos)
        video_lock.release()
    except Exception as exception:
        print_ts('While playing video, caught exception %s' % exception)

def watch_kbd():
    """An infinite loop of waiting for a coin to be inserted.
    Either Player 1 or Player 2 coins are accepted. The Pi
    MUST be connected to an Ultimarc J-Pac, which records coin
    insertions as the characters '5' or '6'."""
    tty.setraw(sys.stdin)
    while True:
        k = sys.stdin.read(1)[0]
        if k == '5' or k == '6':
            coin_inserted()

print_ts('Starting')
Thread(target=watch_kbd).start()
while True:
    play_next_video()
