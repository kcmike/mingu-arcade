## Overview

This is some Python code to make a video player for an old JAMMA
arcade console. It relies on the Ultimarc J-Pac to do video
output and coin acceptor (coin slot) acceptance. It relies on
omxplayer to play the actual videos.

## Licence

Copyright (C) 2018 Michael Burrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see
[GNU licenses](https://www.gnu.org/licenses/).

